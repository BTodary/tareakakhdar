package eg.com.eserve.tareeakakhdar.gui;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import static eg.com.eserve.tareeakakhdar.gui.CustomeTypeFace.getGeneralTypeFace;


public class CustomTextView extends android.support.v7.widget.AppCompatTextView {


    public CustomTextView(Context context) {
        super(context);

        this.setTypeface(getGeneralTypeFace());
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setTypeface(getGeneralTypeFace());
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        this.setTypeface(getGeneralTypeFace());
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }
}
