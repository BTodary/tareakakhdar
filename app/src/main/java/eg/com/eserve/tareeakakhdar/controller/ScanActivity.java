package eg.com.eserve.tareeakakhdar.controller;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.single.PermissionListener;

import eg.com.eserve.tareeakakhdar.R;
import eg.com.eserve.tareeakakhdar.dataBase.DbManager;
import eg.com.eserve.tareeakakhdar.helper.ToolbarCustomization;
import eg.com.eserve.tareeakakhdar.listener.SampleBackgroundThreadPermissionListener;

public class ScanActivity extends AppCompatActivity {

    private DbManager dbManager;

    private PermissionListener cameraPermissionListener;
    private PermissionRequestErrorListener errorListener;

    private String mSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        new ToolbarCustomization(this, getString(R.string.scan), R.layout.include_toolbar_back);

        if (getIntent() == null) return;

        init_permissions_listeners();
        checkPermission();

        if (getIntent().hasExtra("source") && getIntent().getStringExtra("source").equals("sos")) {
            mSource = getIntent().getStringExtra("source");
        } else {
            dbManager = new DbManager();
        }
    }

    private void init_permissions_listeners() {
        errorListener = new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
                Toast.makeText(ScanActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        };
        cameraPermissionListener = new SampleBackgroundThreadPermissionListener(this);
    }

    private void checkPermission() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Dexter.withActivity(ScanActivity.this)
                        .withPermission(Manifest.permission.CAMERA)
                        .withListener(cameraPermissionListener)
                        .withErrorListener(errorListener)
                        .onSameThread()
                        .check();
            }
        }).start();
    }

    public void showPermissionGranted(String permission) {
        startQR();
    }

    public void showPermissionDenied(String permission, boolean isPermanentlyDenied) {
        finish();
    }

    private void startQR() {

        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt(getString(R.string.scan));
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                finish();
            } else {

                if (mSource != null)
                    startActivity(new Intent(this, WidgetDialogActivity.class));
                else
                    dbManager.insertPerson(result.getContents());
                finish();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void showPermissionRationale(final PermissionToken token) {
        new AlertDialog.Builder(this).setTitle("منح أذن")
                .setMessage(R.string.permission_rationale_location)
                .setNegativeButton("إلغاء", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        token.cancelPermissionRequest();
                    }
                })
                .setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        token.continuePermissionRequest();
                    }
                })
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        token.cancelPermissionRequest();
                    }
                })
                .show();
    }
}