package eg.com.eserve.tareeakakhdar.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Created by mac on 4/14/16.
 */
public class DbManager {

    private SQLiteDatabase mDatabase;

    private static DbManager instance;
    private static DbHelper mDatabaseHelper;
    private AtomicInteger mOpenCounter = new AtomicInteger();

    public static synchronized void initializeInstance(DbHelper helper) {
        if (instance == null) {
            instance = new DbManager();
            mDatabaseHelper = helper;
        }
    }

    private static synchronized DbManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException(DbManager.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return instance;
    }

    private synchronized SQLiteDatabase openDatabase() {
        try {
            if (mOpenCounter.incrementAndGet() == 1) {
                // Opening new database
                mDatabase = mDatabaseHelper.getWritableDatabase();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mDatabase;
    }

    private synchronized void closeDatabase() {
        try {
            if (mOpenCounter.decrementAndGet() == 0) {
                // Closing database
                mDatabase.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int clear() {
        int rowId = -1;
        mDatabase = DbManager.getInstance().openDatabase();
        try {
            rowId = mDatabase.delete(Contract.PersonEntry.TABLE_NAME, null, null);
        } finally {
            DbManager.getInstance().closeDatabase(); // correct way
        }
        return rowId;
    }

    // itemId as per web service
    public long insertPerson(String item) {

        if (isItemExistInDb(item)) return -1;

        long id = -1;
        mDatabase = DbManager.getInstance().openDatabase();
        try {
            // Thread 1
            ContentValues contentValue = new ContentValues();
            contentValue.put(Contract.PersonEntry.COLUMN_QR, item);

            id = mDatabase.insert(Contract.PersonEntry.TABLE_NAME, null, contentValue);
        } finally {
            DbManager.getInstance().closeDatabase(); // correct way
        }
        return id;
    }

    public ArrayList<String> getPersons() {
        ArrayList<String> personsArrayList = new ArrayList<>();
        mDatabase = DbManager.getInstance().openDatabase();
        Cursor cursor = null;
        try {
            String countQuery = "SELECT  * FROM " + Contract.PersonEntry.TABLE_NAME;
            cursor = mDatabase.rawQuery(countQuery, null);

            if (cursor != null) {  // looping through all rows and adding to list

                while (cursor.moveToNext()) {
                    String qrStr = cursor.getString(cursor.getColumnIndex(Contract.PersonEntry.COLUMN_QR));
                    personsArrayList.add(qrStr);
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }

            DbManager.getInstance().closeDatabase(); // correct way

        }
        return personsArrayList;
    }


    public int cart_delete_at_id(int StringId) {
        int num_deleted = -1;
        mDatabase = DbManager.getInstance().openDatabase();
        try {
            num_deleted = mDatabase.delete(Contract.PersonEntry.TABLE_NAME, Contract.PersonEntry.COLUMN_QR + "=" + StringId, null);
        } finally {
            DbManager.getInstance().closeDatabase(); // correct way
        }
        return num_deleted;
    }

    public boolean isItemExistInDb(String qr) {
        boolean isItemExist = false;
        mDatabase = DbManager.getInstance().openDatabase();
        Cursor cursor = null;
        try {
            String countQuery = "SELECT  * FROM " + Contract.PersonEntry.TABLE_NAME + " WHERE "
                    + Contract.PersonEntry.COLUMN_QR + " = '" + qr + "'";
            cursor = mDatabase.rawQuery(countQuery, null);

            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String id = cursor.getString(cursor.getColumnIndex((Contract.PersonEntry.COLUMN_QR)));
                    if (id.equals(qr)) {
                        isItemExist = true;
                    }
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }

            DbManager.getInstance().closeDatabase();
        }
        return isItemExist;
    }

    public void clearDb(Context ctx) {
        mDatabase = DbManager.getInstance().openDatabase();
        try {
            ctx.deleteDatabase(DbHelper.DATABASE_NAME);
        } finally {
            DbManager.getInstance().closeDatabase();
        }
    }
}