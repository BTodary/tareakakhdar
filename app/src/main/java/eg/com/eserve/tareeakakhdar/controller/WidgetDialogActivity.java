package eg.com.eserve.tareeakakhdar.controller;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import eg.com.eserve.tareeakakhdar.R;

public class WidgetDialogActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widget_dialog);

        String dialogText = getString(R.string.sosSent);
        TextView txt = findViewById(R.id.w_dialog_txt);
        txt.setText(dialogText);

        Button dismissbutton = findViewById(R.id.w_dismiss_btn);
        dismissbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetDialogActivity.this.finish();
            }
        });
    }

//    private void showFullDisplayDialog() {
//        Dialog dialog = new Dialog(this, R.style.CustomDialog);
//        Window window = dialog.getWindow();
//
//        if (window == null) return;
//
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
//        dialog.setContentView(R.layout.alert_dialog_view);
//
//        WindowManager.LayoutParams wlp = window.getAttributes();
//        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        wlp.height = WindowManager.LayoutParams.MATCH_PARENT;
//        window.setAttributes(wlp);
//
//        Button dialog_positive_btn = dialog.findViewById(R.id.dialog_positive_btn);
//
//        dialog_positive_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                WidgetDialogActivity.this.finish();
//            }
//        });
//
//        dialog.show();
//    }
}
