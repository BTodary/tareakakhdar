package eg.com.eserve.tareeakakhdar.controller;

import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import eg.com.eserve.tareeakakhdar.R;
import eg.com.eserve.tareeakakhdar.dataBase.DbManager;
import eg.com.eserve.tareeakakhdar.helper.DirectionsJSONParser;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String mSource;

    ArrayList<LatLng> markerPoints;
    int mMode = 0;
    final int MODE_DRIVING = 0;
    final int MODE_BICYCLING = 1;
    final int MODE_WALKING = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void test() {

        // Initializing
        markerPoints = new ArrayList<>();

        // Adding new item to the ArrayList
        LatLng Kaaba = new LatLng(21.422693, 39.826208);
        LatLng Muna = new LatLng(21.415090, 39.894506);

        markerPoints.add(Kaaba);
        markerPoints.add(Muna);

        // Draws Start and Stop markers on the Google Map
        drawStartStopMarkers();

        // Checks, whether start and end locations are captured
        if (markerPoints.size() >= 2) {
            LatLng origin = markerPoints.get(0);
            LatLng dest = markerPoints.get(1);

            // Getting URL to the Google Directions API
            String url = getDirectionsUrl(origin, dest);

            DownloadTask downloadTask = new DownloadTask();

            // Start downloading json data from Google Directions API
            downloadTask.execute(url);
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLng(Muna));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11.7f));
        mMap.setBuildingsEnabled(true);
//        mMap.setTrafficEnabled(true);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Kaaba, KSA.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (getIntent() == null) return;
        if (getIntent().hasExtra("source") && getIntent().getStringExtra("source").equals("crowd")) {
            mSource = getIntent().getStringExtra("crowd");

            test();

        } else {
            drawHollyPlaces();
        }
    }

    private void drawHollyPlaces() {

        LatLng Kaaba = new LatLng(21.422693, 39.826208);
        drawMarker(Kaaba, "الكعبة", R.drawable.mecca);

        LatLng Muna = new LatLng(21.415090, 39.894506);
        drawMarker(Muna, "مني", R.drawable.tent);

        LatLng muzdalifa = new LatLng(21.388162, 39.914521);
        drawMarker(muzdalifa, "مزدلفة", R.drawable.prayer);

        LatLng Arafat = new LatLng(21.356004, 39.984030);
        drawMarker(Arafat, "جبل عرفة", R.drawable.mountain);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(Muna));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11f));

//        mMap.setMaxZoomPreference(13.0f);
        mMap.setBuildingsEnabled(true);

//        PolylineOptions rectOptions = new PolylineOptions()
//                .add(Kaaba)
//                .add(Muna)
//                .add(Arafat)
//                .add(muzdalifa);
//
//// Get back the mutable Polyline
//        Polyline polyline = mMap.addPolyline(rectOptions);
//        polyline.setColor(R.color.colorPrimary);
    }

    private void drawMarker(LatLng point, String title, int icon) {
        // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();

        // Setting latitude and longitude for the marker
        markerOptions.position(point);

        // add icon
        markerOptions.icon(BitmapDescriptorFactory.fromResource(icon));


        // Adding marker on the Google Map
        mMap.addMarker(markerOptions).setTitle(title);
    }


    // Drawing Start and Stop locations
    private void drawStartStopMarkers() {

        for (int i = 0; i < markerPoints.size(); i++) {

            // Creating MarkerOptions
            MarkerOptions options = new MarkerOptions();

            // Setting the position of the marker
            options.position(markerPoints.get(i));

            /**
             * For the start location, the color of marker is GREEN and
             * for the end location, the color of marker is RED.
             */
            if (i == 0) {
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            } else if (i == 1) {
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }

            // Add new marker to the Google Map Android API V2
            mMap.addMarker(options);
        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Travelling Mode
        String mode = "mode=driving";

        mMode = 2;

//        if(rbDriving.isChecked()){
//            mode = "mode=driving";
//            mMode = 0 ;
//        }else if(rbBiCycling.isChecked()){
//            mode = "mode=bicycling";
//            mMode = 1;
//        }else if(rbWalking.isChecked()){
//            mode = "mode=walking";
//            mMode = 2;
//        }

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception download url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(4);

                // Changing the color polyline according to the mode
                if (mMode == MODE_DRIVING)
                    lineOptions.color(Color.BLUE);
                else if (i == 0)
                    lineOptions.color(Color.GREEN);
                else if (i == 1)
                    lineOptions.color(Color.RED);
            }

            if (result.size() < 1) {
                Toast.makeText(getBaseContext(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
            mMap.addPolyline(lineOptions);

        }
    }
}