package eg.com.eserve.tareeakakhdar.helper;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import eg.com.eserve.tareeakakhdar.R;

/**
 * Created by bassant on 10/15/17.
 */

public class ToolbarCustomization {

    public ToolbarCustomization(final Activity activity, String title, int layoutId) {

        TextView textView_title_actionbar = activity.findViewById(R.id.title_txt);
        textView_title_actionbar.setText(title);

        if (layoutId == R.layout.include_toolbar_back) {

            ImageView back_img = activity.findViewById(R.id.back_img);
            back_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.finish();
                }
            });

        }
    }

    public ToolbarCustomization(final Activity activity, String title) {

        TextView textView_title_actionbar = activity.findViewById(R.id.title_txt);
        textView_title_actionbar.setText(title);
    }
}
