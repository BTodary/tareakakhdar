package eg.com.eserve.tareeakakhdar.helper;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by shico on 6/21/2016.
 */
public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    /*  private int space;

      public SpacesItemDecoration(int space) {
          this.space = space;
      }

      @Override
      public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
          outRect.left = space;
          outRect.right = space;
          outRect.bottom = space;

          // Add top margin only for the first item to avoid double space between items
          if(parent.getChildAdapterPosition (view) == 0)
              outRect.top = space;
      }*/
    private int spaceTop;
    private int spaceRight;
    private int spaceBottom;
    private int spaceLeft;

    public SpacesItemDecoration(int spaceTop, int spaceRight, int spaceBottom, int spaceLeft) {
        this.spaceTop = spaceTop;
        this.spaceRight = spaceRight;
        this.spaceBottom = spaceBottom;
        this.spaceLeft = spaceLeft;

    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = spaceLeft;
        outRect.right = spaceRight;
        outRect.bottom = spaceBottom;

        outRect.top = spaceTop;
    }
}