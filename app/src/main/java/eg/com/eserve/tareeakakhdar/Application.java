package eg.com.eserve.tareeakakhdar;

import android.content.Context;
import android.support.v7.app.AppCompatDelegate;

import eg.com.eserve.tareeakakhdar.dataBase.DbHelper;
import eg.com.eserve.tareeakakhdar.dataBase.DbManager;


public class Application extends android.app.Application {

    public static final String TAG = Application.class.getSimpleName();
    private static Application mInstance;
    private static Context mContext;

    // enable using vectors on pre lollipop versions
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();
        mInstance = this;

        DbManager.initializeInstance(new DbHelper(getApplicationContext()));
    }

    public static Context getAppContext() {
        return mContext;
    }

    public static synchronized Application getInstance() {
        return mInstance;
    }
}