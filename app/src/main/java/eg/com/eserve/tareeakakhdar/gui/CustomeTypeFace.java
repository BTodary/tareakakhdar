package eg.com.eserve.tareeakakhdar.gui;

import android.graphics.Typeface;

import static eg.com.eserve.tareeakakhdar.Application.getAppContext;

/**
 * Created by bassant on 7/17/17.
 */

public class CustomeTypeFace {

    public static Typeface getGeneralTypeFace() {

        Typeface face = Typeface.createFromAsset(getAppContext().getAssets(), "coconnextarabic_bold.otf");
//        if (LanguageUtil.getAppLanguage(getAppContext()).equals(getAppContext().getString(R.string.ar))) {
//
//            face = Typeface.createFromAsset(getAppContext().getAssets(), "coconnextarabic_bold.otf");
//        }
        return face;
    }

    public static Typeface getBoldTypeFace() {

        Typeface face = Typeface.createFromAsset(getAppContext().getAssets(), "CoconNextArabic-Bold.otf");
//        if (LanguageUtil.getAppLanguage(getAppContext()).equals(getAppContext().getString(R.string.ar))) {
//
//            face = Typeface.createFromAsset(getAppContext().getAssets(), "CoconNextArabic-Bold.otf");
//        }
        return face;
    }

    public static Typeface getLogoTypeFace() {

        Typeface face = Typeface.createFromAsset(getAppContext().getAssets(), "EMBLEM_3.TTF");
//        if (LanguageUtil.getAppLanguage(getAppContext()).equals(getAppContext().getString(R.string.ar))) {
//
//            face = Typeface.createFromAsset(getAppContext().getAssets(), "Hacen Promoter Lt_2.ttf");
//        }
        return face;
    }
}
