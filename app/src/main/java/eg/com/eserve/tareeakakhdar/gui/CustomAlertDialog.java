package eg.com.eserve.tareeakakhdar.gui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import eg.com.eserve.tareeakakhdar.R;

/**
 * Created by bassant on 9/17/2015.
 */
public class CustomAlertDialog extends Dialog {

    private Activity activity;
    private Button dialog_positive_btn, dialog_negative_btn;
    private TextView dialog_msg_txt;
    private AlertDialog mAlertDialog;

    public CustomAlertDialog(Activity activity) {
        super(activity);
        this.activity = activity;

    }


    private void createAlertDialog() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = getLayoutInflater().inflate(R.layout.alert_dialog_view, null);
        dialogBuilder.setView(dialogView);
        mAlertDialog = dialogBuilder.create();
        mAlertDialog.setCancelable(false);

        dialog_msg_txt = dialogView.findViewById(R.id.dialog_msg_txt);
        dialog_positive_btn = dialogView.findViewById(R.id.dialog_positive_btn);
        dialog_negative_btn = dialogView.findViewById(R.id.dialog_negative_btn);
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void alertDialog(String alertMessage) {
        createAlertDialog();

        dialog_msg_txt.setText(alertMessage);
        dialog_positive_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAlertDialog.dismiss();
            }
        });
        if (!activity.isDestroyed()) {
            if (!mAlertDialog.isShowing()) mAlertDialog.show();
        }
    }
}