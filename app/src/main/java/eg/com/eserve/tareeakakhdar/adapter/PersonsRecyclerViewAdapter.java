package eg.com.eserve.tareeakakhdar.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import eg.com.eserve.tareeakakhdar.R;


public class PersonsRecyclerViewAdapter extends RecyclerView.Adapter<PersonsRecyclerViewAdapter.ViewHolder> {

    private final List<String> mValues;
    private Activity mActivity;

    public PersonsRecyclerViewAdapter(Activity activity, List<String> items) {
        mValues = items;
        this.mActivity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_person, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        holder.personId.setText(mValues.get(position));

        holder.trackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=21.626078,39.156124");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                mActivity.startActivity(mapIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;

        TextView personId;
        Button trackBtn;

        ViewHolder(View view) {
            super(view);
            mView = view;

            personId = mView.findViewById(R.id.personId);
            trackBtn = mView.findViewById(R.id.trackBtn);
        }
    }
}
