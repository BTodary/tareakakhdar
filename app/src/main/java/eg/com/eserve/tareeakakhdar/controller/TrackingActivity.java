package eg.com.eserve.tareeakakhdar.controller;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import eg.com.eserve.tareeakakhdar.adapter.PersonsRecyclerViewAdapter;
import eg.com.eserve.tareeakakhdar.R;
import eg.com.eserve.tareeakakhdar.dataBase.DbManager;
import eg.com.eserve.tareeakakhdar.helper.RecyclerLayoutManager;
import eg.com.eserve.tareeakakhdar.helper.ToolbarCustomization;


public class TrackingActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    FloatingActionButton addBtn;

    private DbManager dbManager;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);

        mRecyclerView = findViewById(R.id.tracked_rv);
        addBtn = findViewById(R.id.addBtn);
        new ToolbarCustomization(this, "تتبع الأشخاص", R.layout.include_toolbar);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TrackingActivity.this, ScanActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        dbManager = new DbManager();
        setAdapter();
    }

    private void setAdapter() {
        if (dbManager.getPersons().size() > 0)
            findViewById(R.id.NoPersonsText).setVisibility(View.GONE);

        RecyclerLayoutManager.setRecyclerLinearLayoutManager(mRecyclerView, LinearLayoutManager.VERTICAL);
        mRecyclerView.setAdapter(new PersonsRecyclerViewAdapter(this, dbManager.getPersons()));

    }
}
