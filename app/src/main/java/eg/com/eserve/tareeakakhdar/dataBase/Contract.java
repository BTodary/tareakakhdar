package eg.com.eserve.tareeakakhdar.dataBase;

import android.provider.BaseColumns;

/**
 * Created by mac on 4/14/16.
 */
public class Contract {
    /*
    Inner class that defines the table contents of the Cart table
 */
    public static final class PersonEntry implements BaseColumns {

        public static final String TABLE_NAME = "person";
        public static final String COLUMN_QR = "qr";
    }
}

