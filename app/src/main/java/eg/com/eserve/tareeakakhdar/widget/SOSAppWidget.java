package eg.com.eserve.tareeakakhdar.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import eg.com.eserve.tareeakakhdar.R;
import eg.com.eserve.tareeakakhdar.controller.WidgetDialogActivity;

/**
 * Implementation of App Widget functionality.
 */
public class SOSAppWidget extends AppWidgetProvider {

    private static final String SHOW_DIALOG_ACTION =
            "eg.com.eserve.tareeakakhdar.widget.widgetshowdialog";

    @Override
    public void onUpdate(Context context,
                         AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        prepareWidget(context);
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        // If the intent is the one that signals
        // to launch the modal popup activity
        // we launch the activity
        if (intent.getAction() == null) return;
        if (intent.getAction().equals(SHOW_DIALOG_ACTION)) {

            Intent i = new Intent(context, WidgetDialogActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);

        }

        super.onReceive(context, intent);

    }

    private void prepareWidget(Context context) {

        AppWidgetManager appWidgetManager =
                AppWidgetManager.getInstance(context);

        ComponentName thisWidget =
                new ComponentName(context, SOSAppWidget.class);

        // Fetch all instances of our widget
        // from the AppWidgetManager manager.
        // The user may have added multiple
        // instances of the same widget to the
        // home screen
        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        for (int widgetId : allWidgetIds) {

            RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.soswidget);

            // Create intent that launches the
            // modal popup activity
            Intent intent = new Intent(context, SOSAppWidget.class);
            intent.setAction(SHOW_DIALOG_ACTION);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            // Attach the onclick listener to
            // the widget button
            remoteViews.setOnClickPendingIntent(R.id.sosBtn,
                    pendingIntent);

            appWidgetManager.updateAppWidget(widgetId, remoteViews);

        }
    }

}