package eg.com.eserve.tareeakakhdar.helper;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import eg.com.eserve.tareeakakhdar.R;

import static eg.com.eserve.tareeakakhdar.Application.getAppContext;


/**
 * Created by VisionsTech on 9/22/16.
 */
public class RecyclerLayoutManager {

    public static void setRecyclerLinearLayoutManager(RecyclerView recyclerView, int orientation) {
        PreCachingLinearLayoutManager linearLayoutManager = new PreCachingLinearLayoutManager(getAppContext(), orientation, false);
        int linearSpacingSmall = (int) getAppContext().getResources().getDimension(R.dimen.extra_small_margin);
        int linearSpacingNormal = (int) getAppContext().getResources().getDimension(R.dimen.extra_small_margin);

        SpacesItemDecoration itemDecoration = null;
        if (orientation == LinearLayoutManager.VERTICAL) {
            itemDecoration = new SpacesItemDecoration(0, 0, linearSpacingSmall, 0);
        } else if (orientation == LinearLayoutManager.HORIZONTAL) {
            itemDecoration = new SpacesItemDecoration(0, linearSpacingNormal, 0, 0);
        }
        recyclerView.addItemDecoration(itemDecoration);

        linearLayoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getAppContext()));
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    public static void setRecyclerLinearLayoutManagerWithoutDecor(RecyclerView recyclerView, int orientation) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getAppContext(), orientation, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    public static PreCachingGridLayoutManager setRecyclerGridLayoutManager(RecyclerView recyclerView, int spanCount) {
        int spacingNormal = (int) getAppContext().getResources().getDimension(R.dimen.extra_small_margin);
        int linearSpacingSmall = (int) getAppContext().getResources().getDimension(R.dimen.extra_small_margin);

        PreCachingGridLayoutManager glm = new PreCachingGridLayoutManager(getAppContext(), spanCount);
        glm.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getAppContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(glm);
        SpacesItemDecoration itemDecoration = new SpacesItemDecoration(0, linearSpacingSmall, spacingNormal, linearSpacingSmall);
        recyclerView.addItemDecoration(itemDecoration);

        return glm;
    }

    public static StaggeredGridLayoutManager setRecyclerStaggeredGridLayoutManager(RecyclerView recyclerView, int spanCount) {

        StaggeredGridLayoutManager slm = new StaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.VERTICAL);
        slm.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        recyclerView.setLayoutManager(slm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        return slm;
    }

    public static void setRecyclerGridLayoutManagerWithoutDecor(RecyclerView recyclerView, int spanCount) {
        PreCachingGridLayoutManager gridLayoutManager = new PreCachingGridLayoutManager(getAppContext(), spanCount);
        gridLayoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getAppContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);
    }

//    private static void setRecyclerProperties(Context context, RecyclerView recyclerView, RecyclerView.LayoutManager layoutManager){
//
//
//        RecyclerView.ItemDecoration rvDecor =
//                new HorizontalDividerItemDecoration.Builder(context)
//                        .color(Color.TRANSPARENT)
//                        .sizeResId(R.dimen.lv_divider_height)
//                        .build();
//        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.addItemDecoration(rvDecor);
//    }
}
